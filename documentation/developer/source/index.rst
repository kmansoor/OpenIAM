.. OpenIAM documentation master file, created by
   sphinx-quickstart on Wed Aug  9 14:17:59 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to OpenIAM's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation

   getting_started

   use_cases

   Class Documentation <autodoc.rst>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
