Getting Started
===============

.. toctree::

The prototype OpenIAM does not have a GUI interface. Users can access the
OpenIAM capabilities through input text files called Control files. The
OpenIAM can be run through command line calls with options used to specify the
control file to use.

.. Reference and cite use of Matk here

.. include:: control_file.rst

Output
-------

Output is written to the folder specified in the control file with the
OutputDirectory keyword in the ModelParams section.  Note that in the example
control files the OutputDirectory is a relative path from where the example
file is contained, but the OutputDirectory could also be specified as an
absolute path if desired (e.g., C:\\OpenIAM_Output or ~/OpenIAM_testing).
For each component model of the system Outputs can be specified. When an
output is specified for a forward model the values of that output are written
to a file (output_name.txt) in the output directory. For a stochastic model
(LHS or parstudy analysis) the outputs are included in the results file
(LHS_results.txt or parstudy_results.txt) as well as a file for a statistical summary
of the input parameters and output observations (LHS_statistics.txt or
parstudy_statistics.txt). A copy of the input control file is also written
to the OutputDirectory folder.

Additionally, time series plots can be produced. In the Plots section of the
control file, a plot is created by giving it a label (*P1*, *P2*, etc. in the
example). The label is also used as the name of the image file written to the
output directory (*P1* produces *P1.png*). In the data section (under the plot
label) the name of the output is specified to be plotted. Note that one output
can relate to several time-series, that is CO2_aquifer1 will produce a time
series for the |CO2| leaking into aquifer 1 for all of the wellbores, e.g., if
there are 3 wellbores, there will be three lines on the plot.

Analysis
---------

The OpenIAM uses the Model Analysis ToolKit (MATK) :cite:`MATK` for the basis of its probabilistic
framework.  More information about MATK can be found here:
`http://dharp.github.io/matk/ <http://dharp.github.io/matk/>`_.  The MATK code repository can
be found here: `https://github.com/dharp/matk <https://github.com/dharp/matk>`_.

Parameter input and output interactions can be explored using the *Analysis* section
of the control file. Correlation coefficients can be calculated using the
*CorrelationCoeff* keyword. Parameter sensitivity coefficients for any output
simulation value can be calculated using a Random-Balanced-Design Fourier
Amplitude Sensitivity Test (RBD-Fast) technique. The control file keywords
*SensitivityCoeff*, *MultiSensitivities*, *TimeSeriesSensitivity* can be
used to access different sensitivity coefficient outputs.  See *ControlFile_ex8.yaml*
for details on using the analysis section.

The Sensitivity Analysis is done with the SALib package :cite:`Herman2017`. 
For more information on the RBD-Fast technique see :cite:`TISSOT2012205` and 
:cite:`PLISCHKE2010354`.  While not accessible through the control files, a
scripting interface is provided to a Sobol sensitivity analysis :cite:`SOBOL20093009`.

This section will be expanded in the future.

.. include:: units.rst