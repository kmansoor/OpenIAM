.. _control_file:

Control Files
-------------

Control files are the method of getting user input into the OpenIAM for setting
up a simulation scenario. Control files use a YAML format. Any line in the
control file starting with a pound sign (#) is a comment and will be ignored by
the program. The basic format of the control file is a parameter name followed
by a colon, space, and the value. For objects with several parameters the
object name is followed by a colon and the underlying parameters are listed on
the following lines tabbed in. For example, consider this partial file::

    #OpenIAM Control File example
    #-------------------------------------------------
    ModelParams:
        EndTime: 50
        TimeStep: 1.0
        Analysis: forward
        Components: [SimpleReservoir1,
                     CementedWellbore1]
        OutputDirectory: ../../output_testing/output_{datetime}
        Logging: Debug
    Stratigraphy:
        shale1Thickness:
            min: 500.0
            max: 550.0
            value: 525.0
        shale2Thickness:
            min: 450.0
            max: 500.0
            value: 475.0
        aquifer1Thickness:
            vary: False
            value: 22.4
        reservoirThickness:
            vary: False
            value: 51.2
    WellboreLocations:
        coordx: [100, 540]
        coordy: [100, 630]

Here, the first two lines are comments that are not read by the code.
The third line defines the object *ModelParams* which describes parameters of
the system model. The subsequent lines contain parameters of *ModelParams*.
A *ModelParams* section is required in all OpenIAM control files. The *EndTime*
parameter defines the ending time for the simulation in years (50 years in
the example). The *TimeStep* parameter defines the length of a time step
(1 year in the example). The type of analysis being run is a forward model.
Other possible options for *Analysis* parameter would be *lhs* for Latin
Hypercube Sampling or *parstudy* for a parameter study. The *Components* parameter
is a required entry that contains a list of component model names that are
defined later in the file. The component list will always begin with a square
bracket '[' followed by each of the component names that make up the system
separated by a comma ',' and closed by a square bracket ']'. The next parameter
*OutputDirectory* defines a directory for the output to be written into.
The output directory can be specified with a keyword *{datetime}*.
When a simulation is ran, the *{datetime}* keyword will be replaced with the date
and time of the simulation run. Note that the *{datetime}* keyword is optional: if it is
omitted subsequent runs of the simulation will overwrite past results.
That is, if there is a need to keep all results from re-running an OpenIAM case,
the *{datetime}* keyword will easily facilitate this, if re-running an
OpenIAM case should overwrite previous results, the *{datetime}* keyword should be
omitted. In the output folder the OpenIAM places a copy of the input file, all outputs
from the component models written to text files, and .png images of any
graphics.  The last parameter *Logging* defines what level of logging
information is written out to the logging files. Options for *Logging* levels
are *Debug*, *Info*, *Warning*, and *Error*. *Info* is the default level (if no
*Logging* keyword is given the logging will be set to *Info*) and will give
you a valuable information about when parameters go outside of permitted
ranges and when there are problems in the program. Debug is a good option if
you have problems with your IAM model and want more information to explore the
causes.

The next keyword section of the file is the required *Stratigraphy* section.  In
this section any model parameters related to the stratigraphy of the |CO2| storage site is
defined.  Any parameters for the stratigraphy are defined here with either
a deterministic value or a range to vary over.  A fixed value of any
given parameter can be specified with the *vary: False* and *value: ###*
specification shown here or simply *parameterName: ###*.
The *min* and *max* specification gives a range for the parameter to vary over
if an analysis is done over multiple realizations.  See the :ref:`stratigraphy_component`
section of this document for a list of all available parameters.

The next keyword is *WellboreLocations*.  This keyword defines its own section and
is not part of the *ModelParams* section (it is not intended to be under *ModelParams*).
In this section the x and y coordinates of two leaky wellbores are given.  Coordinates
are assumed to be given in units of meters.  When using the SimpleReserveroir model
the default injection location is at [0, 0], when using a Lookup Table based reservoir
the wellbore locations should fall in the domain of the reservoir simulations.

Continuing this example, the next sections of the input file will define each
of the component models in the component model list specified. The first
component listed in the example is SimpleReservoir1, it will be defined as follows::

    #-------------------------------------------------
    # SimpleReservoir1 is a user defined name for component
    # the type SimpleReservoir is the ROM model name
    #-------------------------------------------------
    SimpleReservoir1:
        Type: SimpleReservoir
        Parameters:
            injRate: 0.1
        Outputs: [pressure,
                  CO2saturation]

This section of the file defines a SimpleReservoir Component model named
*SimpleReservoir1* to be part of the system model. The name *SimpleReservoir1* can
be replaced with any other name defined by user, but will not be a part of the system
model unless it is an element of the components list described in the previous section
*ModelParams*. The *Type* is a parameter that defines the component model to be used and must match up
with one of the component models currently available in the IAM.
The *Parameters* section defines parameters for the component model.
Description of parameters available for the user to specify can be found in
the component model section of the current documentation. The component model
parameters are specified in the same fashion as the Stratigraphy parameters.
The *Outputs* specifies the observations of the component model
that will be output from the simulation.
Please refer to the component models section of this document to see which
parameters and outputs are available for user specification in the control file.

Generally, dynamic (time-varying) input to component models comes from the
output of other connected component models (e.g., the pressure and saturation
as an input to a wellbore leakage model comes from a reservoir model). In some instances
there may be a need to study a component model without the other attached component
models feeding the input. In this case dynamic input can be specified with
the *DynamicParameters* keyword. Under the *DynamicParameters* section each
input name is specified followed by a list of values (enclosed in square brackets [])
of the same length as the number of time points (a value for each time point, including
an initial value).  See *ControlFile_ex7.yaml* for an example of dynamic input.

The next section of the input file is similar to the previous section and defines
the next component model *CementedWellbore1*::

    #-------------------------------------------------
    CementedWellbore1:
        Type: CementedWellbore
        Connection: SimpleReservoir1
        Number: 2
        Parameters:
            logWellPerm:
                min: -14.0
                max: -12.0
                value: -13.0
        Outputs: [CO2_aquifer1,
                  CO2_aquifer2,
                  CO2_atm,
                  brine_aquifer1,
                  brine_aquifer2]

In this part of the example, *CementedWellbore* type component model is
specified. There are two wellbores of this type being added with *Number: 2*,
and their locations are given in the *WellboreLocations* section above.

Unknown wellbore locations can be generated by specifying more wellbores
than the number of known wellbore locations.  In this example, if 4 wellbores
were specified, the first two would be placed at the given locations and the
next two wellbores would be placed randomly.  To control random well placement,
a *RandomWellDomain* section can be specified as::

    RandomWellDomain:
        min: [0, 50]
        max: [100, 200]

This specification will limit the x-coordinate of random wells to be between 0
and 100, and the y-coordinate to be between 50 and 200.  Sampling will be
from a uniform distribution on the domain defined by *min* and *max*.  Random wells are not used in
this first example, see *ControlFile_ex3.yaml* for an example using random well
placement.

The last section of the input file is used to specify a graphical output::

    Plots:
        CO2_Leakage:
            TimeSeries: [CO2_aquifer1, CO2_aquifer2]
        Pressure_plot:
            TimeSeries: [pressure]
            Title: Reservoir Pressure at Wellbore Location

Here, two plots are being requested.  The first plot will illustrate the |CO2| leakage for
both the shallow aquifer and the thief zone aquifer; the second plot will illustrate the
pressures in the reservoir for the two wellbore locations specified earlier in the control file.
*CO2_Leakage* and *Pressure_plot* are the user defined names of the two plots
to be created: these will also be used as the filenames of the figures
saved in the output directory. *TimeSeries* is a keyword that instructs
the program to plot the observation data as a time series plot. The values to be
plotted (*CO2_aquifer1*, *CO2_aquifer2* and *pressure* above) have to be defined
in the control file as outputs from one of the specified component models.
Each plot will have a title corresponding to the values plotted. A user
defined title can be specified with the *Title* keyword in the given plot
section. The |CO2| leakage rates for both aquifers (*CO2_aquifer1* and *CO2_aquifer2*) will be plotted
on the same graph. If each observation is to be plotted on a separate subplot,
the *subplot* keyword with *use* set to True can be specified.  Additionally,
the *ncols* keyword (under *subplot* section) can be used to set the number
of subplot columns to use. The number of rows is controlled by the number
of different values (observations) to plot over the number of columns.
Each subplot will be given a (default) title of the variable plotted.
The default title names can be replaced with the user defined ones by
using the full observation name as a key and the desired title as the value under *subplot* section.

The example file described here can be found in the *examples* directory with
the filename *ControlFile_ex1.yaml*. To run this example, open a command
prompt in the *examples* directory and run the command::

    python ..\source\openiam\openiam_cf.py --file ControlFile_ex1.yaml

*note: use \\ on Windows and / on Mac and Linux*.

Other example control files can be found in the same directory. They can be
run by replacing the file name in the above command with the user specified one.