# -*- coding: utf-8 -*-
"""
The reservoir data interpolator estimates pressure and |CO2| saturation
at the location of interest using precomputed lookup tables.
The calculations are based on linear interpolation for irregular grids.
"""

import logging
import numpy as np
np.set_printoptions(threshold=np.nan)
import os
import scipy.spatial.qhull as qhull
import sys
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

try:
    from openiam import SystemModel
except ImportError as err:
        print('Unable to load IAM class module: '+str(err))


def _blit_draw(self, artists, bg_cache):
    """
    Patch that allows for title and axes changes during animation.

    The original code is taken from:
    https://stackoverflow.com/questions/17558096/animated-title-in-matplotlib
    """
    # Handles blitted drawing, which renders only the artists given instead
    # of the entire figure.
    updated_ax = []
    for a in artists:
        if a.axes not in bg_cache:
            bg_cache[a.axes] = a.figure.canvas.copy_from_bbox(a.axes.figure.bbox)
        a.axes.draw_artist(a)
        updated_ax.append(a.axes)

    # After rendering all the needed artists, blit each axes individually.
    for ax in set(updated_ax):
        ax.figure.canvas.blit(ax.figure.bbox)

# Patch to handle title changes
matplotlib.animation.Animation._blit_draw = _blit_draw

class AnimatedScatterPlot(object):
    def __init__(self, interp_obj, obs_nm='pressure'):
        """
        Constructor method of AnimatedScatterPlot class.

        Create object that would animate the time evolution of the indicated data
        associated with the supplied interpolator.

        :param interp_obj: the ReservoirDataInterpolator object whose data
            is needed to be plotted
        :type interp_obj: ReservoirDataInterpolator object

        :param obs_nm: name of observational data to be shown as animation;
            possible values: 'pressure' and 'saturation'
        :type obs_nm: str

        :returns: AnimatedScatterPlot class object
        """
        self.num_time_points = len(interp_obj.time_points)
        self.num_points = len(interp_obj.points[:,0])

        self.time_points = interp_obj.time_points
        self.points = interp_obj.points
        self.interp_obj = interp_obj
        self.obs_nm = obs_nm

        self.data = {'pressure': self.interp_obj.pressure_data/1.0e+6,
                     'saturation': self.interp_obj.saturation_data}
        self.colorbar_title = {'pressure': 'MPa', 'saturation': ''}
        self.plot_title = {'pressure': 'Pressure data at time t = {:05.2f} years',
                           'saturation': 'Saturation data at time t = {:05.2f} years'}
        self.stream = range(0,self.num_time_points)

        # Setup the figure and axes
        self.fig, self.ax = plt.subplots(figsize=(8,8))

        # Set the x and y limits
        self.ax.axis([np.min(self.points[:,0]), np.max(self.points[:,0]),
                      np.min(self.points[:,1]), np.max(self.points[:,1])])
        self.ax.set_xlabel('x')
        self.ax.set_ylabel('y')
        self.ax.set_aspect('equal', 'datalim')

        # For the colors range we take the min and max of data for all time points
        self.scat = self.ax.scatter(x=self.points[:,0], y=self.points[:,1],
                                    c=np.linspace(np.min(self.data[self.obs_nm]),
                                                  np.max(self.data[self.obs_nm]),self.num_points),
                                    animated=True)
        # Add colorbar
        plt.colorbar(self.scat,ax=self.ax)

        # Plot data at the first available time point
        self.scat.set_array(self.data[self.obs_nm][0,:])

        # Add title of the scatter plot as text
        self.title = self.ax.text(.5, 1.05, self.plot_title[self.obs_nm].format(0),
                                  transform = self.ax.transAxes,
                                  va='center', ha='center', size='large')

        # Setup FuncAnimation
        self.ani = animation.FuncAnimation(self.fig, self.update, self.stream,
                                           init_func = self.setup, blit=True,
                                           interval=250, save_count=self.num_time_points)

    def setup(self):
        """ Return initial state of the plot."""
        return self.scat, self.title

    def update(self, ind):
        """ Update the scatter plot."""

        # Set colors of the scatter plot
        self.scat.set_array(self.data[self.obs_nm][ind])

        # Set description of data
        self.title.set_text(self.plot_title[self.obs_nm].format(self.time_points[ind]))

        # Return the updated artists for FuncAnimation to draw.
        return self.scat, self.title


class ReservoirDataInterpolator(object):
    def __init__(self, name, parent, header_file_dir, time_file, data_file, signature):
        """
        Constructor method of ReservoirDataInterpolator

        :param name: name of interpolator
        :type name: str

        :param parent: name of the system model interpolator belongs to
        :type parent: SystemModel object

        :param header_file_dir: location (directory) of the reservoir
            simulation data that will be used for interpolation
        :type header_file_dir: string

        :param time_file: name of *.csv file to read information about time
            points (in years) at which pressure and |CO2| saturation were calculated
        :type filename: string

        :param data_file: name of *.csv file to read pressure and |CO2| saturation
            data from
        :type filename: string

        :param signature: dictionary of params and their corresponding values
            associated with the given simulation data file
        :type signature: dict()

        :returns: ReservoirDataInterpolator class object
        """
        # Set up attributes of the object
        self.name = name             # name of interpolator
        self.parent = parent         # a system model interpolator belongs to
        self.header_file_dir = header_file_dir  # path to the directory with simulation data files
        self.time_file = time_file   # name of file with time points data
        self.data_file = data_file   # name of file with pressure and saturation data
        self.signature = signature   # dictionary of parameters with values defining this interpolator

        # Create models for each time step and each output type
        self.create_data_interpolators()

        # Log message about creating the interpolator
        logging.debug('ReservoirDataInterpolator created with name {name}'.format(name=name))

    def create_data_interpolators(self):
        """
        Setup attributes storing applicable pressure and saturation data.

        Create pressure and saturation data arrays attributes of the class object
        for use in the model method of the class.
        """
        # Obtain data from csv files
        self.time_points = np.genfromtxt(os.path.join(self.header_file_dir,
            self.time_file), delimiter=",", dtype='f8')      # in years
        data = np.genfromtxt(os.path.join(self.header_file_dir,
            self.data_file), delimiter=",", dtype='f8', skip_header=1)

        # Determine number of time points in the data
        self.num_data_time_points = len(self.time_points)

        # Setup pressure and saturation data
        self.pressure_data = np.transpose(data[:,2:(self.num_data_time_points+2)])
        self.saturation_data = np.transpose(
            data[:,(self.num_data_time_points+2):2*(self.num_data_time_points+1)])
        # Setup x,y points
        self.points = data[:,0:2]

        # Determine triangulation for the data points
        self.triangulation = qhull.Delaunay(self.points)

    def show_data(self, time=None, **kwargs):
        """
        Show data linked to the interpolator.

        :param time: time point at which the data needs to be shown.
            If no time point is provided then animation is created.
            If time point does not coincide with any of the time points
            associated with the interpolator only the extent of the domain
            is shown. By default, time is None.
        :type time: float

        :param kwargs: optional keyword argument specifying obs_nm for the case
            when time is None; contains the name of observation to be shown;
            by default, pressure data is shown.
        :type kwargs: dict
        """
        if time is not None:

            # Find index of time array point
            ind = np.where(self.time_points==time)[0]
            if not type(ind) is list:  # Had to implement this way since ind = 0 evaluates to False but code below should run

                pres_colors = self.pressure_data[ind,:]/1.0e+6
                sat_colors = self.saturation_data[ind,:]

                plt.figure(figsize=(8,8))
                plt.scatter(self.points[:,0],self.points[:,1], c=pres_colors[0,:])
                plt.xlabel('x')
                plt.ylabel('y')
                plt.axes().set_aspect('equal', 'datalim')
                plt.colorbar(label='MPa')
                plt.title('Pressure data at time t = '+str(time)+' years')

                plt.figure(figsize=(8,8))
                plt.scatter(self.points[:,0],self.points[:,1], c=sat_colors[0,:])
                plt.xlabel('x')
                plt.ylabel('y')
                plt.axes().set_aspect('equal', 'datalim')
                plt.colorbar()
                plt.title(r'CO$_2$ saturation data at time t = '+str(time)+' years')
                plt.show()
            else:
                # Show extent of the data
                plt.figure(figsize=(8,8))
                plt.plot(self.points[:,0],self.points[:,1],'o')
                plt.title('Boundaries of the domain')
                plt.xlabel('x')
                plt.ylabel('y')
                plt.axes().set_aspect('equal', 'datalim')
                plt.show()
                logging.warning('Data not available at time t = '+str(time)+' years')
        else:
            if 'obs_nm' in kwargs:
                if not hasattr(self, 'sca_plt'):
                    self.sca_plt = {}
                self.sca_plt[kwargs['obs_nm']] = AnimatedScatterPlot(self, obs_nm=kwargs['obs_nm'])
            else:
                self.sca_plt = {'pressure': AnimatedScatterPlot(self, obs_nm='pressure')}

    def __call__(self, time, vtx=None, wts=None):
        """
        Return pressure and |CO2| saturation at the point of interest.

        :param time: time point (in days) for which the pressure and
            saturation are to be calculated
        :type time: float

        :param vtx: array of indices of simplex vertices which enclose
            the point of interest; array should have a shape (1,3); indices
            do not exceed the number of data points
        :type vtx: numpy.array of int

        :param wts: array of weights of data at simplex vertices which inclose
            the point of interest; array should have a shape (1,3); weights
            values should be between 0 and 1, and sum up to 1
        :type wts: numpy.array of floats

        :returns: out - dictionary of observations;
            keys: ['pressure','CO2saturation']

        """
        # Create dictionary of output
        out = dict()

        # Convert days to years for interpolation
        dtime = time/365.25

        # Check whether time point is within the acceptable range
        if (dtime < self.time_points[0]) or (dtime>self.time_points[-1]):
            range_str = ', '.join((str(self.time_points[0]),str(self.time_points[-1])))

            logging.error('Time point t = '+str(dtime)+' years provided '+
                'by system model is beyond the available time range ['
                 +range_str+'] of the data in '+ self.data_file+'.')
            raise ValueError('Interpolation is requested at the time '+
                'point outside the available range.')

        # Check whether a single point is requested
        if vtx is not None and wts is not None:
            # Import interpolate function
            from grid import interpolate

            # Check whether weights are reasonable; if they are not, then it means
            # that the point at which we need to interpolate is outside the data range
            if np.any(abs(wts)>1.0):
                logging.warning('Input array of weights has invalid values: '+
                                'some or all values are greater than 1.')

            for ind in range(self.num_data_time_points):
                # If time point coincides with one of the data time points
                if dtime == self.time_points[ind]:
                    out['pressure'] = interpolate(self.pressure_data[ind,:], vtx, wts)
                    out['CO2saturation'] = interpolate(self.saturation_data[ind,:], vtx, wts)
                    return out
                # If time point is between the data time points
                elif ((self.time_points[ind]<dtime) and (dtime<self.time_points[ind+1])):
                    p_out1 = interpolate(self.pressure_data[ind,:], vtx, wts)
                    p_out2 = interpolate(self.pressure_data[ind+1,:], vtx, wts)

                    s_out1 = interpolate(self.saturation_data[ind,:], vtx, wts)
                    s_out2 = interpolate(self.saturation_data[ind+1,:], vtx, wts)

                    delta_t = self.time_points[ind+1]-self.time_points[ind]

                    # Interpolate between two data points
                    out['pressure'] = (p_out2-p_out1)/(delta_t)*(dtime-self.time_points[ind])+p_out1
                    out['CO2saturation'] = (s_out2-s_out1)/(delta_t)*(dtime-self.time_points[ind])+s_out1
                    return out
        else:
            for ind in range(self.num_data_time_points):
                # If time point coincides with one of the data time points
                if dtime == self.time_points[ind]:
                    out['pressure'] = self.pressure_data[ind,:]
                    out['CO2saturation'] = self.saturation_data[ind,:]
                    return out
                elif ((self.time_points[ind]<dtime) and (dtime<self.time_points[ind+1])):
                    p_out1 = self.pressure_data[ind,:]
                    p_out2 = self.pressure_data[ind+1,:]

                    s_out1 = self.saturation_data[ind,:]
                    s_out2 = self.saturation_data[ind+1,:]

                    delta_t = self.time_points[ind+1]-self.time_points[ind]

                    # Interpolate between two data points
                    out['pressure'] = (p_out2-p_out1)/(delta_t)*(dtime-self.time_points[ind])+p_out1
                    out['CO2saturation'] = (s_out2-s_out1)/(delta_t)*(dtime-self.time_points[ind])+s_out1
                    return out


if __name__ == "__main__":
    # The code below tests only interpolator and plotting/animation capabilities but it needs a system model
    # for the parent parameter. Thus, we need to create a system model first.

    # Define logging level
    logging.basicConfig(level=logging.WARNING)

    # Create system model
    sm = SystemModel()

    # Create interpolator
    int1 = sm.add_interpolator(ReservoirDataInterpolator(name='int1', parent=sm,
        header_file_dir=os.path.join('..','components','reservoir',
                                    'lookuptables','Kimb_54_sims'),
        time_file='time_points.csv',
        data_file='Reservoir_data_sim02.csv',
        signature={'logResPerm': -13.3, 'reservoirPorosity': 0.25, 'logShalePerm': -18.7}),
        intr_family='reservoir')

    # Print signature of the interpolator
    logging.debug(' Signature of created interpolator is {}'.format(int1.signature))

    # Setup location of interest
    locX,locY = [37478.0,48333.0]

    from grid import interp_weights
    # Calculate weights of the location of interest
    vertices, weights = interp_weights(int1.triangulation,np.array([[locX,locY]]))

    for t in 365.25*10.*np.arange(11):
        out = int1(t,vertices,weights)
        print('At time t = '+str(t/365.25)+' years'+
             ' pressure is '+ str(out['pressure'][0]/1.0e+6)+' MPa'+
             ' and CO2 saturation is ' +str(out['CO2saturation'][0])+'.')

    # Close all figures
    plt.close("all")

    # Show data at 120 years
    int1.show_data(120.0)

    logging.warning("If running code in IPython make sure to switch to the interactive mode to see the animation.")
    # Show pressure and saturation data of interpolator int1 for all available time points
    int1.show_data(obs_nm='pressure')
    int1.show_data(obs_nm='saturation')
    plt.show()

    to_save_figure = False
    # In order to save the figure ffmpeg should be installed.
    # If using Anaconda, in Anaconda prompt enter
    # conda install -c conda-forge ffmpeg
    # Worked on Windows 7
    # Advice is taken from: https://stackoverflow.com/questions/13316397/matplotlib-animation-no-moviewriters-available/14574894#14574894
    if to_save_figure:
        for obs_nm in int1.sca_plt:
            int1.sca_plt[obs_nm].ani.save(obs_nm+'_anim.mp4')

    # Ask whether