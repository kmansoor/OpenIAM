"""
The module contains the solution class for the open wellbore model.

Authors: Diana Bacon, Veronika Vasylkivska
Date: 11/11/2017
"""
import csv
import numpy
from scipy.interpolate import RegularGridInterpolator
import os
import logging


class Solution(object):
    def __init__(self,header_file_directory):
        # read data from file
        file_path = os.path.join(header_file_directory,'NRAP_well_res_ECO2H_Tables.txt')
        with open(file_path) as f:
            ntop, ndepth, ntrans, nbrine, npres, nsat = [int(x) for x in next(f).split()]
            self.top = [int(x) for x in next(f).split()]
            self.depth = [int(x) for x in next(f).split()]
            self.trans = [float(x) for x in next(f).split()]
            self.brine = [float(x) for x in next(f).split()]
            self.dpres = [float(x) for x in next(f).split()]
            self.sat = [float(x) for x in next(f).split()]

            co2Rate = numpy.zeros(shape=(ntop,ndepth,ntrans,nbrine,npres,nsat))
            brineRate = numpy.zeros(shape=(ntop,ndepth,ntrans,nbrine,npres,nsat))

            for t in range(ntop):
                for d in range(ndepth):
                    for r in range(ntrans):
                        for b in range(nbrine):
                            for p in range(npres):
                                for s in range(nsat):
                                    co2Rate[t][d][r][b][p][s], brineRate[t][d][r][b][p][s] = [float(x) for x in next(f).split()]

        # create interpolating functions
        self.co2Interpolator = RegularGridInterpolator((self.top,self.depth,
            self.trans,self.brine,self.dpres,self.sat), co2Rate)
        self.brineInterpolator = RegularGridInterpolator((self.top,self.depth,
            self.trans,self.brine,self.dpres,self.sat), brineRate)

        # Placeholder for the future results
        self.CO2LeakageRates = None
        self.brineLeakageRates = None

    def find(self, inputArray):
        self.CO2LeakageRates = numpy.zeros(2)
        self.brineLeakageRates = numpy.zeros(2)
        # range checking
        inputPres = inputArray[4]
        inputSat = inputArray[5]
        if (inputArray[1] < self.depth[0]):
            inputArray[1] = self.depth[0]
            logging.warning("Clipped minimum depth to %d", self.depth[0])
        if (inputArray[1] > self.depth[-1]):
            inputArray[1] = self.depth[-1]
            logging.warning("Clipped maximum depth to %d", self.depth[-1])
        if (inputArray[2] < self.trans[0]):
            inputArray[2] = self.trans[0]
            logging.warning("Clipped minimum log normalized transmissivity to %d", self.trans[0])
        if (inputArray[2] > self.trans[-1]):
            inputArray[2] = self.trans[-1]
            logging.warning("Clipped maximum log normalized transmissivity to %d", self.trans[-1])
        if (inputArray[3] < self.brine[0]):
            inputArray[3] = self.brine[0]
            logging.warning("Clipped minimum brine concentration to %d", self.brine[0])
        if (inputArray[3] > self.brine[-1]):
            inputArray[3] = self.brine[-1]
            logging.warning("Clipped maximum brine concentration to %.1f", self.brine[-1])
        if (inputArray[4] < self.dpres[0]):
            inputArray[4] = self.dpres[0]
            logging.warning("Scaling below minimum pressure differential of %.1f", self.dpres[0])
        if (inputArray[4] > self.dpres[-1]):
            inputArray[4] = self.dpres[-1]
            logging.warning("Clipped maximum pressure differential to %d", self.dpres[-1])
        if (inputArray[5] < self.sat[0]):
            inputArray[5] = self.sat[0]
            logging.warning("Scaling below minimum saturation of %.2f", self.sat[0])
        if (inputArray[5] > self.sat[-1]):
            inputArray[5] = self.sat[-1]
            logging.warning("Clipped maximum saturation to %d", self.sat[-1])

        # save requested wellbore top depth
        requestedTop = inputArray[0];

        if (requestedTop == 0):
            # return leakage rate to atmosphere
            self.CO2LeakageRates[0] = self.co2Interpolator(inputArray)
            self.brineLeakageRates[0] = self.brineInterpolator(inputArray)
            # set leakage rate to aquifer to zero
            self.CO2LeakageRates[1] = 0.
            self.brineLeakageRates[1] = 0.

            # If CO2 saturation is less than minimum, then linearly interpolate to zero
            if (inputSat < self.sat[0]):
                self.CO2LeakageRates[0] = self.CO2LeakageRates[0] * max(inputSat,0)/self.sat[0]

            # If pressure differential is less than minimum, then linearly interpolate to zero
            if (inputPres < self.dpres[0]):
                self.CO2LeakageRates[0] = self.CO2LeakageRates[0] * max(inputPres,0)/self.dpres[0]
                self.brineLeakageRates[0] = self.brineLeakageRates[0] * max(inputPres,0)/self.dpres[0]
        else:
            # set leakage rate to atmosphere to zero
            self.CO2LeakageRates[0] = 0.0
            self.brineLeakageRates[0] = 0.0

            # Leakage rate with wellbore top at 500 m depth
            inputArray[0] = 500.
            CO2LeakageRate500 = self.co2Interpolator(inputArray)
            brineLeakageRate500 = self.brineInterpolator(inputArray)

            # Leakage rate with wellbore top at 0 m depth
            inputArray[0] = 0.
            CO2LeakageRate0 = self.co2Interpolator(inputArray)
            brineLeakageRate0 = self.brineInterpolator(inputArray)

            # Interpolate or extrapolate to requested wellbore top depth
            self.CO2LeakageRates[1] = CO2LeakageRate0 + (CO2LeakageRate500
                -CO2LeakageRate0)/(500.) * requestedTop
            self.brineLeakageRates[1] = brineLeakageRate0 + (brineLeakageRate500
                -brineLeakageRate0)/(500.) * requestedTop

            # If CO2 saturation is less than minimum, then linearly interpolate to zero
            if (inputSat < self.sat[0]):
                self.CO2LeakageRates[1] = self.CO2LeakageRates[1] * max(inputSat,0)/self.sat[0]

            # If pressure differential is less than minimum, then linearly interpolate to zero
            if (inputPres < self.dpres[0]):
                self.CO2LeakageRates[1] = self.CO2LeakageRates[1] * max(inputPres,0)/self.dpres[0]
                self.brineLeakageRates[1] = self.brineLeakageRates[1] * max(inputPres,0)/self.dpres[0]

# The following if statement needed only if one wants to run
# the script as well as to use it as an importable module
# The code below is mainly used for testing purposes.
if __name__ == "__main__":
    test = 4
    if test == 1:
        top = 0
        depth = 1000
        trans = -1.0
        brine = 0
        dpres = 0.10
        sat = 0.01
        # The answer should be
        # CO2: 0.
        # Brine: 0.

    elif test == 2:
        top = 600
        depth = 2000
        trans = 0.0
        brine = 0.1
        dpres = 1.05
        sat = 0.45
        # The answer should be
        # CO2: 13.3
        # Brine: 6.988

    elif test == 3:
        top = 500
        depth = 4000
        trans = -1.0
        brine = 0.1
        dpres = 20
        sat = 0.12
        # The answer should be
        # CO2: 4.86
        # Brine: 4.22
    elif test == 4:
        # bad parameter to test logging
        top = 500
        depth = 5000
        trans = -1.0
        brine = 0.1
        dpres = 20
        sat = 0.12
        # The answer should be
        # CO2: 4.86
        # Brine: 4.22

    sol = Solution('.')
    inputArray = numpy.array([top,depth,trans,brine,dpres,sat])
    sol.find(inputArray)

    print('CO2: ', sol.CO2LeakageRates)
    print('Brine: ', sol.brineLeakageRates)